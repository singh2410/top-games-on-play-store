#!/usr/bin/env python
# coding: utf-8

# # Google Play Store Game Analysis
# #By- Aarush Kumar
# #Dated: October 13,2021

# In[1]:


from IPython.display import Image
Image(url='https://wallpaperaccess.com/full/1657557.jpg')


# In[2]:


import math
import pandas as pd 
import matplotlib.pyplot as plt
import seaborn as sns
import plotly.express as px
import plotly.graph_objects as go


# In[3]:


filepath = '/home/aarush100616/Downloads/Projects/Top games on Playstore/Data/android-games.csv'
data = pd.read_csv(filepath)


# In[4]:


df = data.copy()


# In[5]:


df


# In[6]:


df.info()


# In[7]:


df.isnull().sum()


# ## Easy Data Visualization

# In[8]:


plt.figure(figsize=(14, 7))
labels=df['category'].value_counts().index
plt.pie(df['category'].value_counts().values,labels=labels,
        explode=[0.15, 0.12, 0.1, 0.08, 0.08, 0.08, 0.08, 0.08, 0.08, 0.08, 0.08, 0.08, 0.08, 0.08, 0.08, 0.08, 0.08],
        autopct='%1.1f%%', startangle=90)
plt.title('Category Pie Chart',fontsize=20,pad=40)
plt.axis('equal')
plt.show()


# In[9]:


def Paid(paid):
    if paid  == 0 : return 'Free'
    else: return 'Costs'
df['paid'] = df.apply(lambda x: Paid(x["paid"]), axis = 1)


# In[10]:


plt.figure(figsize = (10, 7))
fig = px.histogram(df, x = 'paid',
                   title='Free vs Paid games',
                   labels={'paid':'Category'})
#ax = sns.countplot(x = "paid", data=df)
fig.show()


# In[11]:


import plotly.express as px
top_rated = df[0:10]
fig =px.sunburst(
    top_rated,
    path=['title', 'category', 'paid'],
    values='average rating',
    color='average rating',
)
fig.update_layout(
    grid= dict(columns=2, rows=1),
    margin = dict(t=0, l=0, r=0, b=0)
)
fig.show()


# In[12]:


def Install(install):
    if install  == "10.0 M" : return 10000000
    elif install  == "50.0 M" : return 50000000
    elif install  == "5.0 M" : return 5000000
    elif install  == "100.0 M" : return 100000000
    elif install  == "1.0 M" : return 1000000
    elif install  == "500.0 k" : return 500000
    elif install  == "500.0 M" : return 500000000
    elif install  == "100.0 k" : return 100000
    else: return 1000000000
df['installs'] = df.apply(lambda x: Install(x["installs"]), axis = 1)


# In[13]:


fig = px.bar(top_rated,
             x='title',
             y='installs',
             title='What are the most downloaded games?')
fig.update_xaxes(categoryorder = 'total descending')
fig.show()


# As we could have predicted, the most popular games are also the ones most downloaded by users. In this graph we can see that the number of installations (the figure contained in the outermost circle) is almost always corresponding to the ranking (the position in the ranking is indicated by the number corresponding to the game, present in the innermost circle), the only exception is given by Temple Run 2. 
